# syncthing-rpm

VERSION=1.27.4

## CentOS v7

syncthing rpm package for CentOS 7.x

Open Source Continuous File Synchronization

- Download Syncthing rpm file for **centos7** from here:
    - [Syncthing - centos7](https://gitlab.com/rpmbased/syncthing-rpm/-/jobs/artifacts/main/browse?job=run-build)

### Install syncthing on CentOS v7

    yum -y install syncthing*.el7.x86_64.rpm

## CentOS v6

syncthing rpm package for CentOS 6.x

Open Source Continuous File Synchronization

- Download Syncthing rpm file for **centos6** from here:
    - [Syncthing - centos6](https://gitlab.com/rpmbased/syncthing-rpm/-/jobs/artifacts/main/browse?job=run-build6)

### Install syncthing on CentOS v6

    yum -y install syncthing*.el6.x86_64.rpm
