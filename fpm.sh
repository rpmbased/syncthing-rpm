#!/bin/bash

VERSION=$1

export DIR=syncthing-fpm
export DIST="el7"

# You can choose to build a different centos distro
[[ -n "$2" ]] && DIST="$2"

if [[ "${DIST}" == "el7" ]] ; then
    mkdir -p $DIR/usr/bin/
    mkdir -p $DIR/usr/lib/systemd/system/
    mkdir -p $DIR/usr/lib/systemd/user/
    mkdir -p $DIR/usr/share/licenses/syncthing/

    cp syncthing-linux-amd64-v"${VERSION}"/syncthing   $DIR/usr/bin/
    cp syncthing-linux-amd64-v"${VERSION}"/LICENSE.txt $DIR/usr/share/licenses/syncthing/
    cp syncthing-linux-amd64-v"${VERSION}"/etc/linux-systemd/system/*               $DIR/usr/lib/systemd/system/
    cp syncthing-linux-amd64-v"${VERSION}"/etc/linux-systemd/user/syncthing.service $DIR/usr/lib/systemd/user/
elif [[ "${DIST}" == "el6" ]] ; then
    mkdir -p $DIR/usr/bin/
    mkdir -p $DIR/etc/rc.d/init.d/
    mkdir -p $DIR/etc/sysconfig/
    mkdir -p $DIR/var/run/syncthing

    cp syncthing-linux-amd64-v"${VERSION}"/syncthing   $DIR/usr/bin/
    cp syncthing-linux-amd64-v"${VERSION}"/LICENSE.txt $DIR/usr/share/licenses/syncthing/
    cp centos6/syncthing.init      $DIR/etc/rc.d/init.d/syncthing
    cp centos6/syncthing.sysconfig $DIR/etc/sysconfig/syncthing
else
    echo "Supported Distros: el7 or el6"
    exit 1
fi

fpm -n syncthing         \
    -v v"${VERSION}"     \
    -t rpm               \
    -s dir               \
    -C $DIR/             \
    --rpm-dist "${DIST}" \
    -a amd64             \
    --description "Open Source Continuous File Synchronization" \
    --maintainer "Evaggelos Balaskas"                           \
    --url "https://syncthing.net/"                              \
    --license "Mozilla Public License 2.0"

